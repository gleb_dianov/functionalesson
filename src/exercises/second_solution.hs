mySum :: Num a => [a] -> a
mySum = foldl (+) 0

lower :: Ord a => a -> [a] -> [a]
lower limit = filter (< limit)

notLower :: Ord a => a -> [a] -> [a]
notLower limit = filter (>= limit)

testList :: [Integer]
testList = [12,6,27,16,22,23,10,10,21,24]

-- indentation is still important
main :: IO ()
main = do print testList
          putStr "Sum = "
          print (mySum testList)
          putStr "lower than 12: "
          print (lower (head testList) testList)
          putStr "greater or equal to 24: "
          print (notLower (last testList) testList)
