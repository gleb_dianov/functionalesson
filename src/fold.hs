foldl :: (b -> a -> b) -> b -> [a] -> b
foldl op prev (x:xs) = foldl op (op prev x) xs
foldl _  prev []     = prev
