mySum :: Num a => [a] -> a
mySum (x:xs) = x + mySum xs
mySum []     = 0

fac :: (Num a, Eq a) => a -> a
fac 0 = 1
fac n = n * fac (n-1)

lower :: Ord a => a -> [a] -> [a]
lower limit (x:xs) = if x < limit then x : lower limit xs else lower limit xs
lower _     []     = []

notLower :: Ord a => a -> [a] -> [a]
notLower limit (x:xs) = if x >= limit then x : notLower limit xs else notLower limit xs
notLower _     []     = []

sort :: Ord a => [a] -> [a]
sort (x:xs) = sort (lower x xs) ++ [x] ++ sort (notLower x xs)
sort []     = []

testList :: [Integer]
testList = [12,6,27,16,22,23,10,10,21,24]

-- indentation is important
main :: IO ()
main = do print testList
          putStr "Sum = "
          print (mySum testList)
          putStr "6! = "
          print (fac $ head $ tail testList)
          putStr "lower than 12: "
          print (lower (head testList) testList)
          putStr "greater or equal to 24: "
          print (notLower (last testList) testList)
          putStr "sorted: "
          print (sort testList)
