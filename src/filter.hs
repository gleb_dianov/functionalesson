filter :: (a -> Bool) -> [a] -> [a]
filter cond (x:xs) = if cond x then x : filter cond xs else filter cond xs
filter _    []     = []
